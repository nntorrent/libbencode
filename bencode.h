#ifndef BENCODE_H
#define BENCODE_H

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * bencode data types
 * indicates the type of bencode_t
 */
typedef enum {
  BENCODE_INVALID,
  BENCODE_STR,
  BENCODE_INT,
  BENCODE_LIST,
  BENCODE_DICT,
} bencode_type_t;

/* bencode_t represents a bencoded value
 *
 * it has a 'type' which can be checked and a 'value' which should preferably never
 * be accessed directly. The value can be extracted by using one of the helper
 * functions below.
 */
struct _bencode_t;

typedef struct _bencode_t {
  bencode_type_t  type;
  size_t          length;

  union {
    char                        *s;
    long long                    i;
    struct _bencode_t          **l;
  } value;

  /* where the bencoded value is in the original file, if any
   * just a quick hack to later be able to find info_hash easily
   */
  char *data;
  size_t data_length;

} bencode_t;

bencode_t *bencode_new();
void bencode_clear(bencode_t *);

/* bencode_decode decodes any data given to it. If the data is not bencoded properly
 * (i.e. formatted incorrectly), it will return NULL. bencode_t * returned from this
 * function is heap-allocated and needs to be freed by calling bencode_free()
 */
int bencode_decode(char *data, size_t len, bencode_t *);

/* The following batch of functions test for bencoded data formats and return 1 if
 * the data matches expectation and 0 if not.
 *
 * EXAMPLE
 * bencode_is_int("i234e") == 1 -> "i234e" is a properly formatted bencode int
 * bencode_is_int("1234")  == 0 -> "1234"  is not
 *
 * while bencode_is_int and bencode_is_str can accurately assess format,
 * bencode_is_list and bencode_is_dict cannot check for the ending required 'e' and
 * if it's not present, the decoding will fall into an infinite loop or crash. This
 * will be fixed later. For now, just make sure your bencoded string is properly
 * formatted if it contains either lists or dicts.
 */
bool bencode_is_int (char *data, size_t len);
bool bencode_is_str (char *data, size_t len);
bool bencode_is_list(char *data, size_t len);
bool bencode_is_dict(char *data, size_t len);

/* This batch of functions parse either portions or the entirety of bencoded string.
 * It is recommended that these not be used directly as the 'standard' interface for
 * decoding is bencode_decode which should be used, but it's here anyway, so use
 * your own judgement. The take a pointer to a char * and all of them modify the
 * pointer to point to the character after the last parsed character in the string.
 *
 * char **c -> pointer to "i1234efghi"
 * bencode_parse_int(c)
 * c -> pointer to "fghi"
 *
 * WARNING! If you pass a char ** to a heap allocated string, you'll lose it
 */
int bencode_decode_int (char *data, size_t, bencode_t *);
int bencode_decode_str (char *data, size_t, bencode_t *);
int bencode_decode_list(char *data, size_t, bencode_t *);
int bencode_decode_dict(char *data, size_t, bencode_t *);

/*
 * encoding back into original
 */
char *bencode_encode(bencode_t *, size_t *);

char *bencode_encode_int (bencode_t *, size_t *);
char *bencode_encode_str (bencode_t *, size_t *);
char *bencode_encode_dict(bencode_t *, size_t *);
char *bencode_encode_list(bencode_t *, size_t *);

/* prints the bencode in a readable format for debugging purposes
 */
void bencode_print(bencode_t *);

/* bencode_copy copies a bencode_t * to another one and returns the new copy while
 * not modifying the old copy.
 */
int bencode_copy(bencode_t *dst, bencode_t *src);

/* these functions should be self-explanatory
 */
long long bencode_int_get_value(bencode_t *);

/* returns the length of the str
 * NOTE str is NOT null-terminated
 *
 * the length field of the bencode_t type can also be used instead of this
 */
size_t bencode_str_get_length(bencode_t *);

/* returns the value of the str
 * NOTE str is NOT null-terminated
 */
char *bencode_str_get_value(bencode_t *, size_t *);

/* bencode_list_get_length returns the number of elements in the list
 * the length field of the bencode_t * can also be used directly if preferred
 *
 * the length field of the bencode_t type can also be used instead of this
 */
size_t bencode_list_get_length(bencode_t *);

/* bencode_list_get returns a COPY of an item at a certain index. it needs to be
 * bencode_free()'d manually.
 */
int bencode_list_get(bencode_t *, size_t index, bencode_t *result);

/* benccode_get_length returns the number of KEYS in the dictionary
 * it is equivalent to the value of the length field divided by 2
 */
size_t bencode_dict_get_length(bencode_t *);

/* bencode_dict_get returns a COPY of an item with a certain key. it needs to be
 * bencode_free()'d manually.
 */
int bencode_dict_get(bencode_t *, char *key, bencode_t *result);

/* frees up any allocated bencode_t *
 * should be used on EVERY bencode_t * returned from ANY of the above functions
 */
void bencode_free(bencode_t *node);

#ifdef __cplusplus
}
#endif

#endif // BENCODE_H
