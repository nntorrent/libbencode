#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bencode.h"

int
main(int argc, char **argv)
{
  bencode_t *b_int = bencode_new();
  char b_int_test[6];
  strcpy(b_int_test, "i1234e");
  if (bencode_decode(b_int_test, 6, b_int) != 0) {
    fprintf(stderr, "decoding int failed\n");
    bencode_free(b_int);
    return EXIT_FAILURE;
  }

  fprintf(stderr, "testing int get value (must be '1234'): ");
  fprintf(stderr, "%lld\n", bencode_int_get_value(b_int));

  size_t temp_size = 0;
  char *temp = bencode_encode(b_int, &temp_size);
  fprintf(stderr, "reconstructing: '%s'\n", temp);
  free(temp);
  bencode_free(b_int);

  bencode_t *b_str = bencode_new();
  char b_str_test[strlen("5:hello")];
  strcpy(b_str_test, "5:hello");
  if (bencode_decode(b_str_test, strlen("5:hello"), b_str) != 0) {
    fprintf(stderr, "decoding str failed\n");
    bencode_free(b_str);
    return EXIT_FAILURE;
  }

  fprintf(stderr, "testing str get length (must be '5'): ");
  fprintf(stderr, "%lu\n", bencode_str_get_length(b_str));
  fprintf(stderr, "testing str get value (must be 'hello'): ");
  bencode_print(b_str);

  temp = bencode_encode(b_str, &temp_size);
  fprintf(stderr, "reconstructing: '%s'\n", temp);
  free(temp);
  bencode_free(b_str);

  fprintf(stderr, "decoding list, should be ['spam', 4, ['hm', 5], 'hi']\n");
  bencode_t *b_list = bencode_new();
  char b_list_test[24];
  strcpy(b_list_test, "l4:spami4el2:hmi5ee2:hie");
  if (bencode_decode(b_list_test, 24, b_list) != 0) {
    fprintf(stderr, "decoding list failed\n");
    return EXIT_FAILURE;
  }
  bencode_print(b_list);

  temp = bencode_encode(b_list, &temp_size);
  fprintf(stderr, "reconstructing: '%s'\n", temp);
  free(temp);
  bencode_free(b_list);

  bencode_t *b_dict = bencode_new();
  int b_dict_test_len = strlen("d2:hii1e5:helloli2323423eli12eli12ee2:hie10:hellohelloee");
  char b_dict_test[b_dict_test_len];
  strcpy(b_dict_test, "d2:hii1e5:helloli2323423eli12eli12ee2:hie10:hellohelloee");
  if (bencode_decode(b_dict_test, b_dict_test_len, b_dict) != 0) {
    fprintf(stderr, "decoding dict failed\n");
    return EXIT_FAILURE;
  }

  fprintf(stderr, "testing dict {hi -> 1, hello -> [2323423, [12, [12], hi], hellohello]}\n");
  bencode_print(b_dict);

  temp = bencode_encode(b_dict, &temp_size);
  fprintf(stderr, "reconstructing: '%s'\n", temp);
  free(temp);
  bencode_free(b_dict);

  return 0;
}
