SRCS=bencode.c test.c
OBJS=$(addsuffix .o, $(basename $(SRCS)))
EXECUTABLE=test
LIBNAME=libbencode.so

CFLAGS=-c -Wall -std=c11 -fPIC -g -mno-tbm
#EXEC_LDFLAGS=-L. -lbencode -Wl,-rpath,$(pwd)
LIB_LDFLAGS=-shared -Wl,-soname,$(LIBNAME)

ifeq ($(shell uname -s),FreeBSD)
	CC=clang
else
	CC=gcc
endif

.SUFFIXES: .c

all: $(EXECUTABLE) $(LIBNAME)

test: $(OBJS)
	$(CC) $^ -o $@

lib: $(LIBNAME)

$(LIBNAME): bencode.o
	$(CC) $(LIB_LDFLAGS) $^ -o $@

clean:
	rm -rf $(OBJS) $(EXECUTABLE) $(LIBNAME)
