#include "bencode.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int bencode_decode_collection(char *data, size_t, bencode_t *);

bencode_t *
bencode_new()
{
  bencode_t *ben = malloc(sizeof(bencode_t));
  ben->type = BENCODE_INVALID;
  ben->length = 0;
  ben->value.i = 0;
  ben->data = NULL;
  ben->data_length = 0;

  return ben;
}

void
bencode_clear(bencode_t *ben)
{
  if (ben->type == BENCODE_STR) {
    if (ben->value.s) {
      free(ben->value.s);
    }
  }

  if ((ben->type == BENCODE_LIST || ben->type == BENCODE_DICT) &&
      ben->value.l) {
    for (size_t i = 0; i < ben->length; i++)
      bencode_free(ben->value.l[i]);

    free(ben->value.l);
  }

  if (ben->data) {
    free(ben->data);
  }

  ben->type = BENCODE_INVALID;
  ben->data = NULL;
  ben->value.i = 0;
  ben->length = 0;
  ben->data_length = 0;
}

int
bencode_decode(char *data, size_t len, bencode_t *ben)
{
  if (bencode_is_int(data, len))
    return bencode_decode_int(data, len, ben);

  if (bencode_is_str(data, len))
    return bencode_decode_str(data, len, ben);

  if (bencode_is_list(data, len))
    return bencode_decode_list(data, len, ben);

  if (bencode_is_dict(data, len))
    return bencode_decode_dict(data, len, ben);

  return -1;
}

bool
bencode_is_int(char *data, size_t len)
{
  // iXXXXXXe
  if (data[0] != 'i')
    return 0;

  char *end = memchr(data, 'e', len);
  if (end == NULL) return 0;

  for (int i = 1; data+i < end; i++)
    if (!isdigit(data[i]))
      return false;

  return true;
}

bool
bencode_is_str(char *data, size_t len)
{
  // ###:XXXXXX
  char *first_section_end = memchr(data, ':', len);
  if (!first_section_end)
    return false;

  int num_digits = 0;
  for (; data+num_digits < first_section_end; num_digits++)
    if (!isdigit(data[num_digits]))
      return false;

  if (len - num_digits - 1 <= 0) {
    return false;
  }

  /* sscanf expects a null-terminated string */
  int length;
  *first_section_end = '\0';
  if (sscanf(data, "%d", &length) != 1) {
    *first_section_end = ':';
    return false;
  }
  *first_section_end = ':';

  if (len < num_digits + 1 + length) {
    return false;
  }

  return true;
}

bool
bencode_is_list(char *data, size_t len)
{
  if (data[0] != 'l')
    return false;
  return true;
}

bool
bencode_is_dict(char *data, size_t len)
{
  if (data[0] != 'd')
    return false;
  return true;
}

int
bencode_decode_int(char *data, size_t len, bencode_t *ben)
{
  /*
   * format: iXXXXXe
   */

  long long ret;

  if (!bencode_is_int(data, len)) {
    return -1;
  }

  /* sscanf expects a null-terminated string */
  char *first_section_end = memchr(data, 'e', len);
  *first_section_end = '\0';
  if (sscanf(data + 1, "%lld", &ret) != 1) {
    *first_section_end = 'e';
    return -1;
  }
  *first_section_end = 'e';

  ben->type = BENCODE_INT;
  ben->length = 0;
  ben->value.i = ret;
  ben->data_length = (char *) (memchr(data, 'e', len) + 1) - data;
  ben->data = malloc(ben->data_length);
  memcpy(ben->data, data, ben->data_length);

  return 0;
}

int
bencode_decode_str(char *data, size_t len, bencode_t *ben)
{
  if (!bencode_is_str(data, len)) {
    return -1;
  }

  ben->type = BENCODE_STR;

  /* sscanf expects a null-terminated string */
  char *first_section_end = memchr(data, ':', len);
  *first_section_end = '\0';
  if (sscanf(data, "%ld", &ben->length) != 1) {
    *first_section_end = ':';
    return -1;
  }
  *first_section_end = ':';

  char *str_section = memchr(data, ':', len);
  int int_section_len = str_section - data;
  str_section++;

  /*
   * no \0 should be appended as there may be null bytes in the middle of
   * the string
   */
  ben->value.s = malloc(ben->length);
  memcpy(ben->value.s, str_section, ben->length);

  ben->data_length = int_section_len + 1 + ben->length;
  ben->data = malloc(ben->data_length);
  memcpy(ben->data, data, ben->data_length);

  return 0;
}

int
bencode_decode_collection(char *data, size_t len, bencode_t *ben)
{
  size_t max_size = 2;

  ben->value.l = malloc(sizeof(bencode_t *) * max_size);

  char *mdata = data + 1; // get past the 'l' or 'd'
  size_t mlen = len - 1;
  size_t size;

  for (size = 0; *mdata != 'e'; size++) {
    if (size >= max_size) {
      max_size += 2;
      ben->value.l = realloc(ben->value.l, max_size * sizeof(bencode_t *));
    }

    bencode_t *temp = bencode_new();
    if (bencode_decode(mdata, mlen, temp) != 0) {
      bencode_free(temp);
      return -1;
    }

    ben->value.l[size] = temp;
    mdata += temp->data_length;
    mlen -= temp->data_length;
  }

  ben->length = size;

  ben->data_length = (mdata + 1) - data;
  ben->data = malloc(ben->data_length);
  memcpy(ben->data, data, ben->data_length);

  return 0;
}

int
bencode_decode_list(char *data, size_t len, bencode_t *ben)
{
  int ret = bencode_decode_collection(data, len, ben);
  ben->type = BENCODE_LIST;
  return ret;
}

int
bencode_decode_dict(char *data, size_t len, bencode_t *ben)
{
  int ret = bencode_decode_collection(data, len, ben);
  ben->type = BENCODE_DICT;
  return ret;
}

void
bencode_print(bencode_t *ben)
{
  if (ben->type == BENCODE_INT)
    printf("int: %lld\n", ben->value.i);

  if (ben->type == BENCODE_STR)
    printf("str: %.*s\n", (int) ben->length, ben->value.s);

  if (ben->type == BENCODE_LIST) {
    printf("list:\n");

    for (size_t i = 0; i < ben->length; i++)
      bencode_print(ben->value.l[i]);
    printf("END\n");
  }

  if (ben->type == BENCODE_DICT) {
    printf("dict:\n");

    for (size_t i = 0; i+1 < ben->length; i+=2)
    {
      printf("key:\n");
      bencode_print(ben->value.l[i]);
      printf("value:\n");
      bencode_print(ben->value.l[i+1]);
    }
    printf("END\n");
  }
}

void
bencode_free(bencode_t *node)
{
  if (node->type == BENCODE_STR && node->value.s)
    free(node->value.s);

  if ((node->type == BENCODE_LIST || node->type == BENCODE_DICT) &&
       node->value.l) {
    for (size_t i = 0; i < node->length; i++)
      bencode_free(node->value.l[i]);

    free(node->value.l);
  }

  if (node->data) {
    free(node->data);
  }

  free(node);
}

int
bencode_copy(bencode_t *dst, bencode_t *src)
{
  dst->type = src->type;
  dst->length = src->length;
  dst->data_length = src->data_length;
  dst->data = malloc(dst->data_length);
  memcpy(dst->data, src->data, dst->data_length);

  switch (src->type) {
    case BENCODE_INT:
      dst->value.i = src->value.i;
      break;

    case BENCODE_STR:
      dst->value.s = malloc(dst->length);
      memcpy(dst->value.s, src->value.s, dst->length);
      break;

    case BENCODE_LIST:
    case BENCODE_DICT:
      dst->value.l = malloc(sizeof(bencode_t *) * dst->length);
      memset(dst->value.l, 0, sizeof(bencode_t *) * dst->length);

      for (size_t i = 0; i < dst->length; i++) {
        dst->value.l[i] = bencode_new();
        bencode_copy(dst->value.l[i], src->value.l[i]);
      }

      break;

    default:
      return -1;
  }

  return 0;
}

int
bencode_list_get(bencode_t *node, size_t index, bencode_t *ben)
{
  if (index < bencode_list_get_length(node)) {
    return bencode_copy(ben, node->value.l[index]);
  } else {
    return -1;
  }
}

int
bencode_dict_get(bencode_t *node, char *key, bencode_t *ben)
{
  for (size_t i = 0; i < node->length - 1; i+=2) {
    bencode_t *t = bencode_new();
    if (bencode_list_get(node, i, t) != 0 ||
        t->type != BENCODE_STR) {
      bencode_free(t);
      return -1;
    }

    if (t->length != strlen(key)) {
      bencode_free(t);
      continue;
    }

    if (memcmp(t->value.s, key, t->length) != 0) {
      bencode_free(t);
      continue;
    }

    bencode_clear(t);

    if (bencode_list_get(node, i+1, t) != 0) {
      bencode_free(t);
      return -1;
    }

    if (bencode_copy(ben, t) != 0) {
      bencode_free(t);
      return -1;
    }

    bencode_free(t);
    return 0;
  }

  return -1;
}

size_t
bencode_list_get_length(bencode_t *node)
{
  return node->length;
}

size_t
bencode_dict_get_length(bencode_t *node)
{
  return node->length / 2;
}

long long
bencode_int_get_value(bencode_t *node)
{
  return node->value.i;
}

size_t
bencode_str_get_length(bencode_t *node)
{
  return node->length;
}

char *
bencode_str_get_value(bencode_t *node, size_t *len)
{
  *len = bencode_str_get_length(node);
  char *ret = malloc(*len);
  memcpy(ret, node->value.s, *len);
  return ret;
}

char *
bencode_encode(bencode_t *node, size_t *s)
{
  if (s == NULL)
    return NULL;

  if (node->type == BENCODE_INT)
    return bencode_encode_int(node, s);
  else if (node->type == BENCODE_STR)
    return bencode_encode_str(node, s);
  else if (node->type == BENCODE_LIST)
    return bencode_encode_list(node, s);
  else if (node->type == BENCODE_DICT)
    return bencode_encode_dict(node, s);
  else
    return NULL;
}

char *
bencode_encode_int(bencode_t *node, size_t *size)
{
  if (node->type != BENCODE_INT)
    return NULL;

  char *res = malloc(25); // long long is 20 digits at max + i + e + \0
  snprintf(res, 25, "i%llde", node->value.i);

  *size = strlen(res);
  return res;
}

char *
bencode_encode_str(bencode_t *node, size_t *size)
{
  if (node->type != BENCODE_STR)
    return NULL;

  char *len = malloc(22); // long long is 20 digits at max + : + \0
  snprintf(len, 22, "%ld:", node->length);

  char *res = malloc(node->length + strlen(len) + 1);
  memcpy(res, len, strlen(len));
  memcpy(res+strlen(len), node->value.s, node->length);

  *size = strlen(len) + node->length;
  free(len);
  return res;
}

char *
bencode_encode_collection(bencode_t *node, size_t *size)
{
  if (node->type != BENCODE_LIST &&
      node->type != BENCODE_DICT)
    return NULL;

  size_t max_size = 8;

  char *res = malloc(max_size);

  if (node->type == BENCODE_LIST)
    res[0] = 'l';
  else if (node->type == BENCODE_DICT)
    res[0] = 'd';
  *size = 1;

  for (size_t i = 0; i < node->length; i++) {
    size_t temp_size = 0;
    char *temp = bencode_encode(node->value.l[i], &temp_size);

    while (*size + temp_size >= max_size) {
      max_size *= 2;
      res = realloc(res, max_size);
    }

    memcpy(res+(*size), temp, temp_size);
    *size += temp_size;
    free(temp);
  }

  res[*size] = 'e';
  (*size)++;

  return res;
}

char *
bencode_encode_dict(bencode_t *node, size_t *size)
{
  return bencode_encode_collection(node, size);
}

char *
bencode_encode_list(bencode_t *node, size_t *size)
{
  return bencode_encode_collection(node, size);
}

